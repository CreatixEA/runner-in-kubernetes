FROM gitlab/gitlab-runner
RUN apt update
RUN apt install docker.io --yes
RUN apt install maven --yes
RUN apt clean